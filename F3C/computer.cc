/*
 * F3C.cc
 *
 *  Created on: 30 mai 2018
 *      Author: hlakhlef
 */

//
// This file is part of an OMNeT++/OMNEST simulation example.

//
// This file is distributed WITHOUT ANY WARRANTY. See the file
// `license' for details on this and other legal matters.
//
#include <stdio.h>
#include <stdio.h>
#include <string.h>
#include <omnetpp.h>
#include "Msg_m.h"
#include "MsgTerm_m.h"
#include "cl.h"
#include <time.h>
#include <math.h>

int max(int a,int b);

int max(int a,int b)
{
  if(a>=b)
    return a;
  else
    return b;
}
using namespace omnetpp;

/**
 * Derive the Txc1 class from cSimpleModule. In the Tictoc1 network,
 * both the `tic' and `toc' modules are Txc1 objects, created by OMNeT++
 * at the beginning of the simulation.
 */

class computer1 : public cSimpleModule
{

protected:
  int finished[10000];
  int notcolored[1000000];
  int neighterm[1000];
  int usedcolor[13];
  int allcolors[3000000];
  int Deltaparent;

  int term[10000];
  int colored;
  int h, cond=0;
  int mycolors[30];
  int parentcolors[30];
  float m;
  float maxcl;
  int setProposedColors[300000];
  int mcolors[13];
  int mult ;
  // The following redefined virtual function holds the algorithm.
  virtual void initialize() override;
  virtual void handleMessage(cMessage *msg) override;
};

// The module class needs to be registered with OMNeT++
Define_Module(computer1);

void computer1::initialize()
{

  finished[this->getId()]=0; // if  finished[this->getId()]=1 the node is done and can not send messages.
  term[this->getId()]=0; // if term[this->getId()]=1 the has received a term message from all its children.

  h=0; //s=0;  // h :  l'horloge
  //this->m=1;
  m=1;
  mult=1;
  Deltaparent=11;
  maxcl=0;
  colored=0;

  for(int j=0; j<13;j++)
    {
      //  notcolored[j]=1;
      //   usedcolor[j]=0;
      mycolors[j]=-1;
      //   mcolors[j]=0;
      //
    }

  for(int j=0; j<this->gateCount()/2;j++)
    {
      notcolored[j]=1;
      //   usedcolor[j]=-1;

    }

  for(int j=0; j<13;j++)
    {
      //  notcolored[j]=1;
      usedcolor[j]=0;
      //     mycolors[j]=-1;
      mcolors[j]=0;
      //
    }

  for(int j=0; j<1000000;j++)
    {

      allcolors[j]=-3;

    }
  for(int j=0; j<100;j++)
    {

      neighterm[j]=0;

    }

  for (int i = 0; i <30000; i++)
    {
      setProposedColors[i]=-3;

    }

  if (this->getId()== 2) {

    Msg *msg_color[this->gateCount()/2];
    mycolors[1]=1;

    float x=  ceil(((float)(this->gateCount()/2)/m))+1;

    int v, f;
    //     EV <<"x="<<(int)x ;

    int t=0;
    for ( f = 2; f<(int)x+1; f++) {
      for (v = t; v<m+t; v++) {
        setProposedColors[(3*v)]=f;
      }

      t=t+m;
    }

    if (((this->gateCount()/2)%(int)(m))!=0)
      {
        int mod = (this->gateCount()/2)%(int)(m);
        // EV <<  "pas divisible"<<mod;
        for (int w = 0; w <(m-mod); w++)
          {
            //  EV <<"v="<<  v<< "\n";
            setProposedColors[(3*(this->gateCount()/2))-w]=-3;
            //      setProposedColors[(3*w)+1]=f-1;
          }

      }

    for (int i = 0; i <25; i++)

      EV <<  setProposedColors[i];

    for (int i = 0; i < this->gateCount()/2; i++) {

      if ( (notcolored[i])==-1)  continue;

      msg_color[i] = new Msg("COLOR");

      for (int k = 0; k <25; k++)
        msg_color[i]->setProposedColors(k, setProposedColors[k]);
      //    usedcolor[cl]=1;
      msg_color[i]->setMycolorss(1,1);
      msg_color[i]->setMaxcl(f-1);

      msg_color[i]->setDestination(i);
      msg_color[i]->setKind(1);
      msg_color[i]->setDeltaParent(this->gateCount()/2);
      notcolored[i]=-1;

      send(msg_color[i], "out", i);
      nbmsg();
      finished[this->getId()]=1; //new

    }
    finished[this->getId()]=1;

  }

  //  for (int i = 0; i <25; i++)

  //   EV <<  setProposedColors[i];
  cMessage *pMsg = new cMessage("ClockEvent");
  scheduleAt(simTime()+1, pMsg);

}
void computer1::handleMessage(cMessage *msg)
{
  if (msg->isSelfMessage())
    {

      for (int k = 0; k < 100; k++) {

        if (mycolors[k]==k)
          EV << this->getName()<<" with color " << k <<  "\n";

      }

      if (finished[this->getId()]==1){

        for (int k = 0; k < 100; k++) {

          if (mycolors[k]==k)
            incr();
        }

        goto C;

      }
      h++;

      if ((this->gateCount()==2) || (term[this->getId()]==1))

        {
          float f= ceil(Deltaparent/m)+1;

          for (int k = 0; k < 13; k++) {

            if ((h%((int)f+2)==mycolors[k]) && (  finished[this->getId()]==0)){

              MsgTerm *Term = new MsgTerm("TERM");
              Term->setKind(2);

              for (int p = 0; p < 13; p++) {

                if (mycolors[p]==p)
                  Term->setMycolors(p, p);

              }

              //     send(Term, "out", 0);

              finished[this->getId()]=1;

              break;
            }
          }
          scheduleAt(simTime()+1.0, msg);

        G:
          goto C;
        }

      Msg *msg_color[this->gateCount()/2];

      float f= ceil(Deltaparent/m);

      int pp=0;
      if (colored){
        float x=  max (ceil(((float)(this->gateCount()/2)/m))+1,  ceil(((float)((maxcl)/m))));

        int v1, f1;
        EV <<"x="<<(int)x<< "vide" <<maxcl<<"\n";
        maxcl=x;
        int t=1;

        for ( f1 = 1; f1<(int)x+1; f1++) {

          if (mycolors[f1]==f1) continue ;

          for (v1 = t; v1<m+t; v1++) {
            EV <<"vvvv="<<f1<<mcolors[f1]<< "\n";

            if (mcolors[f1]>=m) {pp=1; goto R; }
            mcolors[f1]++;
            setProposedColors[(3*v1)]=f1;
          }
        R:
          t=t+m-pp;
          pp=0;
        }

        if ((maxcl-(ceil(((float)(this->gateCount()/2)/m))+1))!=0)
          {
            int mod = (maxcl-(ceil(((float)(this->gateCount()/2)/m))+1));
            EV <<  "MOOOOOD" <<mod<< "\n";
            for (int w = 1; w <=mod; w++)
              {
                setProposedColors[((3*v1)-(3*w))]=-3;
              }
          }
        if (((this->gateCount()/2)%(int)(m))!=0)
          {
            int mod = (this->gateCount()/2)%(int)(m);

            for (int w = 0; w <(m-mod); w++)
              {
                setProposedColors[(3*(this->gateCount()/2+1-pp))-w]=-3;
                if (mycolors[f1-1]==f1-1) continue ;
              }
          }

        for (int i = 0; i <35; i++)

          EV <<  setProposedColors[i];

        colored=0;
      }

      for (int k = 0; k < 13; k++) {
        if ((h%((int)f+2)==mycolors[k]))
          {
            for (int i = 0; i < this->gateCount()/2; i++) {

              if ((i==0)|| (notcolored[i])==-1)  continue;

              msg_color[i] = new Msg("COLORZ");

              msg_color[i]->setMaxcl(maxcl);

              for (int k = 0; k <25; k++)
                {
                  msg_color[i]->setProposedColors(k, setProposedColors[k]);

                  if (setProposedColors[k]>=0)
                    usedcolor[k]=m;
                }
              for (int p = 0; p < 13; p++) {

                if (mycolors[p]==p)
                  msg_color[i]->setMycolorss(p, p);

              }

              msg_color[i]->setDestination(i);
              msg_color[i]->setKind(1);
              msg_color[i]->setDeltaParent(this->gateCount()/2);
              notcolored[i]=-1;

              send(msg_color[i], "out", i);
              nbmsg();
              finished[this->getId()]=1;

            }

            break;
          }
      }

      scheduleAt(simTime()+1.0, msg);

    }
 C:

  if((msg->getKind()==1))
    {

      colored=1;
      Msg *ttmsg = check_and_cast<Msg *>(msg);
      //  EV << ttmsg->getSenderGateId()<<" gates id " <<  "\n";
      if ((ttmsg->getSenderGateId()-2621440) ==(ttmsg->getDestination())) {
        notcolored[0]=-1;
        Deltaparent=ttmsg->getDeltaParent();
        if (maxcl<ttmsg->getMaxcl())
          maxcl= ttmsg->getMaxcl();

        for (int b = 0; b < 10000; b++) {
          allcolors[b]=ttmsg->getProposedColors(b);

          if (ttmsg->getMycolorss(b)>0)// 0;
            {
              setProposedColors[ttmsg->getMycolorss(b)%3]=ttmsg->getMycolorss(b);
              //        usedcolor[ttmsg->getMycolorss(b)]++;
            }

        }

        int  neighborcolors [100];
        for (int p = 0; p < 100; p++) {
          neighborcolors[p] =ttmsg->getMycolorss(p);
          if (p==neighborcolors[p])
            {
              usedcolor[p]++;
              mcolors[p]++;

            }

          //   EV << p<<  mcolors[p] <<"mcolo " << "\n";
        }

        EV <<" MAXXX" <<maxcl << "\n";

        if ((ttmsg->getDestination()==1) && (this->gateCount()==2)){

          for (int d = 1;d <maxcl+1; d++) {
            int bolt =1;

            for (int k = 0; k <100; k++)
              {
                if  ((allcolors[k]==d)|| (usedcolor[d]==m))
                  {
                    bolt=0;

                    break;
                  }

              }

            if (bolt)
              {
                mycolors[d]=d; //ici
                if (mult)
                  {
                    //     if (mycolors[k]==k)
                    incr();
                  }

              }
            bolt=0;

          }

        }

      }

      for (int k = 0; k < 100; k++) {

        if (mycolors[k]==k)
          EV << this->getName()<<" with color " << k << "\n";

      }

      for (int k = 3*ttmsg->getDestination(); k < 3+ (ttmsg->getDestination()*3); k++) {

        if (ttmsg->getProposedColors(k)!=-3)

          {
            mycolors[ttmsg->getProposedColors(k)]=ttmsg->getProposedColors(k);

          }

      }

      for (int p = 0; p < 10000; p++) {

        if (ttmsg->getProposedColors(p)!=-3)
          {

            usedcolor[p]=m;
            //

          }

      }

      for (int k = 0; k < 100; k++) {

        if (mycolors[k]==k)
          EV << this->getName()<<" with color " << k <<  "\n";

      }

      if ((this->gateCount()==2)&& (finished[this->getId()]==0))

        {

          float f= ceil(Deltaparent/m);

          for (int k = 0; k < 13; k++) {
            if ((h%((int)f+1)==mycolors[k]) && (finished[this->getId()]==0)){

              MsgTerm *Term = new MsgTerm("TERM");
              Term->setKind(2);
              //   send(Term, "out", 0);
              finished[this->getId()]=1;

            }
          }
        }
    }

  // A:

  // }

  else
    {
      if (msg->getKind()==2)
        {

          neighterm[msg->getArrivalGateId()-1048576]=1;

          //

          int boo=1;

          for (int i=1; i<this->gateCount()/2; i++)
            {
              if (neighterm[i]==0)
                {
                  boo=0;
                  break;
                }
            }

          MsgTerm *ttmsg = check_and_cast<MsgTerm *>(msg);

          for (int k = 0; k < 100; k++) {

            if (ttmsg->getMycolors(k)>0)
              usedcolor[ttmsg->getMycolors(k)]= m;
          }

          if ((boo==1)&&(this->getId() != 2) ){

            term[this->getId()]=1;

            if (this->gateCount()/2>2)
              {

                for (int d = 1;d <maxcl; d++) {

                  EV <<"ppppp " << usedcolor[d]<< d<<"\n";
                  if  (usedcolor[d]==m) continue;

                  mycolors[d]=d; //ici
                  if (mult)
                    {

                    }
                  //   ;
                }
              }
          }
        }
    }
}
