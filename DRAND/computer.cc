/*
 * test01.cc
 *
 *  Created on: 30 mai 2018
 *      Author: hlakhlef
 */

//
// This file is part of an OMNeT++/OMNEST simulation example.

//
// This file is distributed WITHOUT ANY WARRANTY. See the file
// `license' for details on this and other legal matters.
//
#include <stdio.h>
#include <stdio.h>
#include <string.h>
#include <string.h>
#include <omnetpp.h>
#include "Msg_m.h"

#include "cl.h"
#include <time.h>
#include <math.h>

#define N 500
extern int N2 [N][N];
extern int colored [N];
int max(int a,int b);

int max(int a,int b)
{
  if(a>=b)
    return a;
  else
    return b;
}
using namespace omnetpp;

/**
 * Derive the Txc1 class from cSimpleModule. In the Tictoc1 network,
 * both the `tic' and `toc' modules are Txc1 objects, created by OMNeT++
 * at the beginning of the simulation.
 */

class computer1 : public cSimpleModule
{

protected:
  char* colors[12]={"red","blue","yellow","black","pink","brown","green","white","purple", "magenta","cyan","DarkGreen"};
  int received_grant=-1;
  bool scheduled_msg=false;
  int state=1 ; // 1: IDLE, 2: REQUEST, 3: RELEASE, 4:GRANT .
  int mycolor=0,used_color[N]={0};

  int t=240;
  int id;

  int onehopcolor[N]={0};
  int received_colors=0;

  virtual void initialize() override;
  virtual void handleMessage(cMessage *msg) override;
};

// The module class needs to be registered with OMNeT++
Define_Module(computer1);

void computer1::initialize()
{

  id = this->getId()-2;

  N2[(int)this->par("p")][id]=1;
  N2[id][(int)this->par("p")]=1;

  for(int i=0; i<t;i++)
    {
      if (N2[this->par("p")][i]==1)
        {
          N2[i][id]=2;
          N2[id][i]=2;
        }

    }
  N2[id][id]=0;
  printf("------------------------ \n");
  for(int i=0; i<t;i++)
    {
      for(int j=0; j<t;j++)
        printf ("%d ", N2[i][j]);
      printf("\n");
    }
  printf("------------------------");

  cMessage *pMsg = new cMessage("ClockEvent");
  scheduleAt(simTime()+1, pMsg);
  scheduled_msg=true;
}

void computer1::handleMessage(cMessage *msg)
{
  EV<<"received grant:"<<received_grant<<"\n";
  Msg *msg_fail, *msg1, *msg_release,*msg_rej,*msg_grant;
  cMessage  *pMsg;
  srand(time(NULL));
  if (msg->isSelfMessage())
    { //start protocol
      scheduled_msg=false;
      // received_grant=-1;
      printf("----------- \nid:%d \n state:%d \n mycolor%d \n-----------\n",id,state,mycolor);
      EV<<"start messages \n";

      //int r= rand()%2;
      int r= intuniform(0,1,0);
      printf("rand %d\n",   r );

      if (r==1)
        {
          int max=1;
          int cpt=0;
          printf("avant boucle for \n");
          for (int i = 0; i <t; i++)
            {

              if ((N2[id][i]!=0) and (colored[i]==0))
                {

                  for (int j = 0; j <t; j++)
                    if (N2[i][j]!=0) cpt++;

                  if (max<cpt)    max=cpt;
                  ;
                  cpt=0;
                }
            }
          printf("id:%d,  max=%d \n",id,max);

          int r2=intuniform(1,max,0);
          printf("rrrrrrr = %d \n", r2%max);

          if (r2==1)
            {

              state=2;
              received_grant=0;

              for (int i=0; i<this->gateCount()/2; i++)
                {
                  printf("boucle.....\n");
                  Msg *msg_request = new Msg("REQUEST");

                  msg_request->setKind(2);
                  msg_request->setSource(id);
                  send(msg_request, "out", i);
                  incr();
                  EV<<"send request :"<<id<<":->"<<received_grant<<"\n";

                  received_grant++;

                }

              // A :

            }
          printf("************** \n");

        }

    }

  else { //receive messages from other nodes

    EV<<"reception \n";
    switch (msg->getKind()) {
    case 2: //receive request
      if((state==1)or(state==3)){
        state=4;
        msg_grant = new Msg("GRANT");//();
        msg_grant->setKind(4);
        msg_grant->setSource(id);
        msg_grant->setColor(mycolor);

        for (int p = 0; p < t; p++) {
          msg_grant->setOne_hopecolors(p,onehopcolor[p]);
        }

        EV<<"sendergate:"<<msg->getArrivalGate()->getIndex()<<"\n";
        send(msg_grant,"out",msg->getArrivalGate()->getIndex());
        incr();
      }
      else
        {
          if((state==2)or(state==4)){// send reject

            msg_rej = new Msg("REJECT");
            msg_rej->setKind(5);
            msg_rej->setSource(id);
            EV<<"senderreject:"<<msg->getArrivalGate()->getIndex()<<"\n";
            send(msg_rej,"out",msg->getArrivalGate()->getIndex());
            incr();
          }
        }
      break;
      
    case 3:

      msg1 = check_and_cast<Msg *>(msg);
      EV<<"receive release message arrival from "<<msg1->getSource()<<"color:"<<msg1->getColor()<< "\n";
      used_color[msg1->getColor()]=1;
      onehopcolor[msg1->getColor()]=1;
      for (int var = 0; var < msg1->getOne_hopecolorsArraySize(); ++var) {
        used_color[var]=msg1->getOne_hopecolors(var);
      }

      if (mycolor==0)
        state=1;
      else
        {

          if(msg1->getType()!=2) {
            state=3;
            EV<<"release message arrival from "<<msg1->getSource()<<"\n";
            Msg *msg2;
            for (int var = 0; var < this->gateCount()/2; var++) {
              msg2=new Msg();
              msg2->setType(2);
              msg2->setKind(7);
              msg2->setSource(msg1->getSource());
              msg2->setDestination(msg1->getDestination());
              msg2->setColor(msg1->getColor());
              send(msg2,"out",var);
              incr();
              EV<<"send 2 hope message arrival from "<<msg1->getSource()<<"\n";

            }
          }
        }
      break;
      
    case 4:// receive grant
      if(received_grant>0) received_grant--;
      //   printf("received grant:%d \n",received_grant);

      msg1 = check_and_cast<Msg *>(msg);
      EV<<"receive grant color:"<<msg1->getColor()<<"\n";
      used_color[msg1->getColor()]=1;

      for (int var = 0; var < t; var++) {
        if(!used_color[var])
          used_color[var]=msg1->getOne_hopecolors(var);
        EV<<"receive grant two hope colors: "<<used_color[var]<<"\n";
      }

      if(received_grant==0) {
        EV<<"receive grant color2:"<<msg1->getColor()<<"\n";
        int m=1;
        while(used_color[m]==1) {
          m=m+1;
        }

        EV<<"chosen color m="<<m<<"  "<<used_color[m]<<" \n";
        mycolor=m;
        colored[id]=1;
        this->getDisplayString().setTagArg("i",1,colors[mycolor]);

        state=3;
        EV<<"--------my color  node: " <<id<<" :"<<mycolor<<"\n";
        for (int var = 0; var < this->gateCount()/2; var++) {

          msg_release = new Msg("RELEASE");
          msg_release->setKind(3);
          msg_release->setColor(mycolor);
          msg_release->setSource(id);
          EV<<"senderrelease:"<<var<<"\n";
          send(msg_release,"out",var);
          incr();

        }
      }
      break;

    case 5://receive reject

      received_grant=-1;
      for (int var = 0; var < this->gateCount()/2; var++) {
        msg_fail = new Msg("FAIL");
        msg_fail->setKind(6);
        msg_fail->setSource(id);
        EV<<"senderfail:"<<msg->getArrivalGate()->getIndex()<<"\n";
        send(msg_fail,"out",var);
        incr();
      }
      state=1;
      break;
      
    case 6://receive fail

      if(colored[id]) state=3;
      else state=1;
      break;
      
    default:
      break;
    }
  }
  EV<<"end receive:"<<id<<":->"<<received_grant<<"\n";

  if((!scheduled_msg)&&(received_grant==-1)&&(!mycolor))  {
    EV<<"prepare selfmsg :"<<id<<":->"<<received_grant<<"\n";

    pMsg = new cMessage("ClockEvent");

    scheduleAt(simTime()+1, pMsg);
    scheduled_msg=true;
    EV<<"end prepare selfmsg :"<<id<<":->"<<received_grant<<"\n";

  }
}
