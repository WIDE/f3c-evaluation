# F3C-Evaluation

This projects contains the code of the [OMNeT++](https://omnetpp.org/) evaluation of the F3C-algorithm proposed in the publication "Vertex Coloring with Communication Constraints in Synchronous Broadcast Networks".

The files have been tested with OMNeT++ v. 5.3.

## To execute F3C :

- First we need to put the folder F3C  in the folder samples  of OMNeT++.

- Run the protocl arb.c that creates a tree of size n 200 (you can change the value of n in the file arb.c) and write it in the file arbre.txt in F3C .

- Run the project F3C on OMNeT++ (run as OMNeT++ application).


## To execute DRAND :

- First we need to put the folder DRAND  in the folder samples of OMNeT++.

- Run the protocl arb.c that creates a tree and write it in the file arbre.txt in DRAND.

- Run the project DRAND on OMNeT++ (run as OMNeT++ application).
